use std::rc::Rc;
use std::cell::RefCell;

mod basic;
use crate::basic::traits::{Part, Container};
use crate::basic::structs::{Component, Resistor};

fn main() {
    let mut c0 = Component::new();
    let mut rc1= Rc::new(RefCell::new(Resistor::new()));
    let mut rc2= Rc::new(RefCell::new(Resistor::new()));

    c0.add_part(rc1.clone());
    c0.add_part(rc2.clone());
    c0.connect_parts(rc1.clone(), rc2.clone());
    println!("{}", c0.are_parts_connected(rc1.clone(), rc2.clone()));
    // c0.connect_parts(rc3, rc4);
    // println!("{}", rc3.borrow().get_id());
}