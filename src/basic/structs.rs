use std::rc::Rc;
use std::cell::RefCell;

use super::traits::{Part, Container};
use crate::basic::graph::Graph;
use std::ops::{Deref, DerefMut};

pub(crate) struct Component {
    parts : Vec<Rc<RefCell<dyn Part>>>,
    id: usize,
    name: String,
    graph : Graph,
}

impl Part for Component {
    fn get_id(&self) -> usize {
        return self.id;
    }

    fn set_id(&mut self, id: usize) {
        self.id = id;
    }

    fn get_name(&self) -> String {
        return self.name.clone();
    }

    fn set_name(&mut self, name: String) {
        self.name = name.clone();
    }
}

impl Container for Component {
    fn add_part(&mut self, mut part: Rc<RefCell<dyn Part>>) {
        self.graph.add_next_node();
        part.deref().borrow_mut().deref_mut().set_id(self.graph.size()-1);
        self.parts.push(part);
    }

    fn connect_parts(&mut self, part0: Rc<RefCell<dyn Part>>, part1: Rc<RefCell<dyn Part>>) {
        self.graph.add_edge(part0.borrow().get_id(), part1.borrow().get_id());
    }

    fn are_parts_connected(&self, part0: Rc<RefCell<dyn Part>>, part1: Rc<RefCell<dyn Part>>) -> bool{
        return self.graph.contains_edge(part0.borrow().get_id(), part1.borrow().get_id());
    }

    fn size(&self) -> usize {
        return self.parts.len();
    }
}

impl Component {
    pub fn new() -> Component {
        return Component{
            parts: vec![],
            id: 0,
            name: "".to_string(),
            graph: Graph::new(),
        }
    }

    pub fn new_with_name(name : String) -> Component {
        return Component{
            parts: vec![],
            id: 0,
            name,
            graph: Graph::new(),
        }
    }
}

pub(crate) struct Resistor {
    id: usize,
    name: String,
    resistance: f64,
}

impl Part for Resistor {
    fn get_id(&self) -> usize {
        return self.id;
    }

    fn set_id(&mut self, id: usize) {
        self.id = id;
    }

    fn get_name(&self) -> String {
        return self.name.clone();
    }

    fn set_name(&mut self, name: String) {
        self.name = name.clone();
    }
}

impl Resistor {
    pub fn new() -> Resistor {
        return Resistor{
            id: 0,
            name: "".to_string(),
            resistance: 0.0
        };
    }

    pub fn get_resistance(& self) -> f64 {
        return self.resistance;
    }

    pub fn set_resistance(& mut self, resistance : f64) {
        self.resistance = resistance;
    }
}

pub(crate) struct Capacitor {
    id: usize,
    name: String,
    capacitance: f64,
}

impl Part for Capacitor {
    fn get_id(&self) -> usize {
        return self.id;
    }

    fn set_id(&mut self, id: usize) {
        self.id = id;
    }

    fn get_name(&self) -> String {
        return self.name.clone();
    }

    fn set_name(&mut self, name: String) {
        self.name = name.clone();
    }
}

impl Capacitor {
    pub fn new() -> Capacitor {
        return Capacitor{
            id: 0,
            name: "".to_string(),
            capacitance: 0.0
        };
    }

    pub fn get_capacitance(& self) -> f64 {
        return self.capacitance;
    }

    pub fn set_capacitance(& mut self, capacitance : f64) {
        self.capacitance = capacitance;
    }
}

pub(crate) struct Coil {
    id: usize,
    name: String,
    inductance: f64,
}

impl Part for Coil {
    fn get_id(&self) -> usize {
        return self.id;
    }

    fn set_id(&mut self, id: usize) {
        self.id = id;
    }

    fn get_name(&self) -> String {
        return self.name.clone();
    }

    fn set_name(&mut self, name: String) {
        self.name = name.clone();
    }
}

impl Coil {
    pub fn new() -> Coil {
        return Coil{
            id: 0,
            name: "".to_string(),
            inductance: 0.0
        };
    }

    pub fn get_inductance(& self) -> f64 {
        return self.inductance;
    }

    pub fn set_inductance(& mut self, inductance : f64) {
        self.inductance = inductance;
    }
}

pub(crate) struct Node {
    id: usize,
    name: String,
}

impl Part for Node {
    fn get_id(&self) -> usize {
        return self.id;
    }

    fn set_id(&mut self, id: usize) {
        self.id = id;
    }

    fn get_name(&self) -> String {
        return self.name.clone();
    }

    fn set_name(&mut self, name: String) {
        self.name = name.clone();
    }
}

pub(crate) struct Source {
    left: Node,
    right: Node,
    id: usize,
    name: String,
    voltage: f64,
    r_i : f64
}

impl Part for Source {
    fn get_id(&self) -> usize {
        return self.id;
    }

    fn set_id(&mut self, id: usize) {
        self.id = id;
    }

    fn get_name(&self) -> String {
        return self.name.clone();
    }

    fn set_name(&mut self, name: String) {
        self.name = name.clone();
    }
}

impl Source {
    pub fn new() -> Source {
        return Source{
            left: Node {id: 0, name: "".to_string() },
            right: Node {id: 0, name: "".to_string()},
            id: 0,
            name: "".to_string(),
            voltage: 0.0,
            r_i: 0.0
        };
    }

    pub fn get_voltage(& self) -> f64 {
        return self.voltage;
    }

    pub fn set_voltage(& mut self, voltage : f64) {
        self.voltage = voltage;
    }

    pub fn r_i(& self) -> f64 {
        return self.voltage;
    }

    pub fn set_r_i(& mut self, r_i : f64) {
        self.r_i = r_i;
    }
}

#[cfg(test)]
mod tests {
    use super::{Component, Resistor, Coil, Container};
    use std::cell::RefCell;
    use std::rc::Rc;

    #[test]
    fn test_add_part() {
        let mut c = Component::new();
        let r = Rc::new(RefCell::new(Resistor::new()));
        let c0 = Rc::new(RefCell::new(Coil::new()));
        c.add_part(r.clone());
        c.add_part(c0.clone());
        assert_eq!(c.size(), 2);
    }

    #[test]
    fn test_connection() {
        let mut c = Component::new();
        let r = Rc::new(RefCell::new(Resistor::new()));
        let c0 = Rc::new(RefCell::new(Coil::new()));
        c.add_part(r.clone());
        c.add_part(c0.clone());
        c.connect_parts(r.clone(), c0.clone());
        assert!(c.are_parts_connected(r.clone(), c0.clone()));
    }
}



