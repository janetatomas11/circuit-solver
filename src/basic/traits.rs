use std::cell::RefCell;
use std::rc::Rc;

// Trait, which must be implemented by every struct that is to be added into circuit
pub trait Part {
    fn get_id(&self) -> usize;
    fn set_id(& mut self, id : usize);
    fn get_name(&self) -> String;
    fn set_name(& mut self, name : String);
}

// Trait, which must be implemented by every struct that is to be containing some parts (is composed from them, can be circuit for example.)
pub trait Container {
    fn add_part(&mut self, part: Rc<RefCell<dyn Part>>);
    fn connect_parts(& mut self, part0:  Rc<RefCell<dyn Part>>, part1:  Rc<RefCell<dyn Part>>);
    fn are_parts_connected(&self, part0:  Rc<RefCell<dyn Part>>, part1:  Rc<RefCell<dyn Part>>) -> bool;
    fn size(& self) -> usize;
}
