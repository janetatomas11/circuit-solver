
pub(crate) struct Graph {
    nodes: Vec<Vec<usize>>,
}

impl Graph {
    pub fn new() -> Graph {
        return Graph{ nodes: vec![]};
    }
    pub fn add_next_node(&mut self) {
        self.nodes.push(vec![]);
    }

    pub fn contains_edge(& self, id0: usize, id1: usize) -> bool {
        let result = self.nodes.get(id0).expect("").contains(&id1);
        return result;
    }

    pub fn add_edge(&mut self, id0: usize, id1: usize) {
        self.nodes.get_mut(id0).expect("").push(id1);
        self.nodes.get_mut(id1).expect("").push(id0);
    }

    pub fn size(& self) -> usize {
        return self.nodes.len();
    }

}

#[cfg(test)]
mod tests {
    use super::Graph;

    #[test]
    fn test_add_next_node() {
        let mut g = Graph::new();
        assert_eq!(g.size(), 0);
        g.add_next_node();
        assert_eq!(g.size(), 1);
    }

    #[test]
    fn test_edge() {
        let mut g = Graph::new();
        g.add_next_node();
        g.add_next_node();
        g.add_edge(0, 1);
        assert!(g.contains_edge(0, 1));
    }
}